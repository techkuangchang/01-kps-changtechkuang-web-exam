import React, { Component } from 'react'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Table } from 'react-bootstrap';

export default class App extends Component {
  constructor(){
    super()
    this.state = {
        books : [
            {
                id: 1,           
                title: "មនុស្សពីរនាក់នៅជិតគ្នា",           
                publishedYear: 2012,           
                isHiding: false
            },
            {
                id: 2,           
                title: "គង់ហ៊ាន",           
                publishedYear: 2015,           
                isHiding: false
            },
            {
                id: 3,           
                title: "បុរសចេះថ្នាំពិសពស់",           
                publishedYear: 2018,           
                isHiding: false
            },
            {
                id: 4,           
                title: "អណ្តើកនិងស្វា",           
                publishedYear: 2019,           
                isHiding: false
            },
        ],
    };
  }

  render() {
    let books = this.state.books.map((book) => (
          <div>
            <Table striped bordered hover>
            <thead>
                <tr>
                <th>#</th>
                <th>Title</th>
                <th>PublishedYear</th>
                <th>Action</th>
                </tr>
               
            </thead>
            </Table>
        </div>
    ));
    return (
        <div>
          {books}
        </div>
    )
  }
}

